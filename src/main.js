import {createApp} from "vue";
import VueCookies from 'vue-cookies'

import router from "@/router/router";
import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';

import App from "@/App";

const app = createApp(App)
app.use(router)
app.use(ElementPlus)
app.use(VueCookies)
app.mount('#app')

import { createRouter, createWebHistory } from 'vue-router';

import HomePage from "@/components/Home";
import HelloWorld from "@/components/HelloWorld";
import LoginPage from "@/components/Login";
import ClassComponent from "@/components/ClassComponent";
import CompetitionComponent from "@/components/CompetitionComponent";
import AdminHome from "@/components/admin/AdminHome";
import AdminCompetition from "@/components/admin/AdminCompetition";

const routes = [
    {path: "/home", component: HomePage},
    {path: "/hello", component: HelloWorld},
    {path: "/login", component: LoginPage},
    {path: "/class", component: ClassComponent},
    {path: "/competition", component: CompetitionComponent},
    {path: "", redirect: "/login"},

    {path: "/admin/home", component: AdminHome},
    {path: "/admin/competition", component: AdminCompetition}
];

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

export default router;
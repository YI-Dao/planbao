# PlanBao

为了统一展示，前后端代码放在一个仓库的不同分支进行管理
txx 分支为后端代码分支
main 分支为前端代码分支

后端服务IP地址：116.62.6.122:12123
swagger接口文档地址：http://116.62.6.122:12123/swagger-ui.html

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
